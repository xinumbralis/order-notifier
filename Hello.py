"""Hello Analytics Reporting API V4."""

import argparse

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

import httplib2
from oauth2client import client
from oauth2client import file
from oauth2client import tools

import time
import sys, traceback

import pyaudio  
import wave  

SCOPES = ['https://www.googleapis.com/auth/analytics.readonly']
DISCOVERY_URI = ('https://analyticsreporting.googleapis.com/$discovery/rest')
KEY_FILE_LOCATION = '8399694b0e78.p12'
SERVICE_ACCOUNT_EMAIL = 'check-for-orders@grahams-test-project.iam.gserviceaccount.com'
VIEW_ID = '137201319'


def initialize_analyticsreporting():
  """Initializes an analyticsreporting service object.

  Returns:
    analytics an authorized analyticsreporting service object.
  """

  credentials = ServiceAccountCredentials.from_p12_keyfile(
    SERVICE_ACCOUNT_EMAIL, KEY_FILE_LOCATION, scopes=SCOPES)

  http = credentials.authorize(httplib2.Http())

  # Build the service object.
  analytics = build('analytics', 'v4', http=http, discoveryServiceUrl=DISCOVERY_URI)

  return analytics


def get_report(analytics, label):
  # Use the Analytics Service Object to query the Analytics Reporting API V4.
  return analytics.reports().batchGet(
      body={
        'reportRequests': [
        {
          'viewId': VIEW_ID,
          'dateRanges': [{'startDate': '30daysAgo', 'endDate': 'today'}],
          'metrics': [{'expression': 'ga:totalEvents'}],
          'dimensions' : [
            {'name': 'ga:eventCategory'},
            {'name': 'ga:eventAction'},
            {'name': 'ga:eventLabel'},
          ],
          'filtersExpression': 'ga:eventLabel==' + label
        }]
      }
  ).execute()


def print_response(response, filename):
  """Parses and prints the Analytics Reporting API V4 response"""

  filename = filename.lower()

  for report in response.get('reports', []):
    columnHeader = report.get('columnHeader', {})
    dimensionHeaders = columnHeader.get('dimensions', [])
    metricHeaders = columnHeader.get('metricHeader', {}).get('metricHeaderEntries', [])
    rows = report.get('data', {}).get('rows', [])

    for row in rows:
      dimensions = row.get('dimensions', [])
      dateRangeValues = row.get('metrics', [])

      for header, dimension in zip(dimensionHeaders, dimensions):
        #print header + ': ' + dimension
        pass

      for i, values in enumerate(dateRangeValues):
        #print 'Date range (' + str(i) + ')'
        for metricHeader, value in zip(metricHeaders, values.get('values')):
          output = metricHeader.get('name') + ': ' + value
          file = open(filename + '.txt', 'r')
          contents = file.read()
          file.close()
          with open(filename + '.txt', 'w+') as file:
            if contents != output:
              print 'SOMETHING CHANGED!'
              print 'Was: ' + contents + 'Now: ' + output
              play_audio(filename.lower())
            else:
              print '[' + time.strftime("%d-%m-%y %H:%M:%S") + '] ' + filename + ': ' + value
            file.write(output)
            file.truncate()
            file.close()

def play_audio(filename):
  chunk = 1024
  f = wave.open(r"./" + filename +".wav","rb")
  p = pyaudio.PyAudio()  
  stream = p.open(format = p.get_format_from_width(f.getsampwidth()),  
                channels = f.getnchannels(),  
                rate = f.getframerate(),  
                output = True)  
  data = f.readframes(chunk)  

  while data:  
    stream.write(data)  
    data = f.readframes(chunk)  

  stream.stop_stream()  
  stream.close()  

  p.terminate()  

def main():
  try:
    analytics = initialize_analyticsreporting()
    starttime=time.time()
    while True:
      print_response(get_report(analytics, 'Success'), 'Success')
      time.sleep(10)
      print_response(get_report(analytics, 'Failed'), 'Failed')
      time.sleep(10)
  except KeyboardInterrupt:
    print "Aww, going to sleep"
  except Exception:
    traceback.print_exc(file=sys.stdout)
  sys.exit(0)

if __name__ == '__main__':
  main()


